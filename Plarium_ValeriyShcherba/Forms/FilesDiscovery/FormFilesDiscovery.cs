﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Security.Permissions;
using System.Xml;
using Plarium_ValeriyShcherba.Scripts.App;
using Plarium_ValeriyShcherba.Scripts.Data.Constants;
using Plarium_ValeriyShcherba.Scripts.App.Services;

namespace Plarium_ValeriyShcherba.Scripts.Forms.FilesDiscovery
{
    public partial class FilesDiscovery : Form
    {
        private string _pathDirectory = String.Empty;
        private string _pathXML = String.Empty;

        private ThreadManager _threadManager;

        public FilesDiscovery()
        {
            _threadManager = AppContext.Instance.ThreadManager;

            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(FilesDiscovery_FormClosing);
        }

        void FilesDiscovery_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.FormClosing -= FilesDiscovery_FormClosing;

            _threadManager.StopAllThreads();
        }

        #region Button_Handlers

        private void ButtonSelectDirectory_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = folderBrowserDialog.ShowDialog();

            if (dialogResult != null)
            {
                _pathDirectory = folderBrowserDialog.SelectedPath;
                fromDirectoryLabel.Text = _pathDirectory;
            }
        }

        private void ButtonSelectXML_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = AppConstants.FILE_FILTER_XML;

            DialogResult dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult != null)
            {
                _pathXML = saveFileDialog.FileName;
                toDirectoryLabel.Text = _pathXML;
            }
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_pathDirectory))
            {
                MessageBox.Show(MessageConstants.MESSAGE_NOT_EXIST_DIRECTORY, MessageConstants.MESSAGE_ERROR);
                return;
            }
            if (string.IsNullOrEmpty(_pathXML))
            {
                MessageBox.Show(MessageConstants.MESSAGE_NOT_EXIST_XML, MessageConstants.MESSAGE_ERROR);
                return;
            }

            _threadManager.WaitFillHandle = new ManualResetEvent(false);

            _threadManager.WriteXmlThread.Start(_pathXML);
            _threadManager.TreeThread.Start(TreeView);
            _threadManager.DetectFilesThread.Start(_pathDirectory);
        }

        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

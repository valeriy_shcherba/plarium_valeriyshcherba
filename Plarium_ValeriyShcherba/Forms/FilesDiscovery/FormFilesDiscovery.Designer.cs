﻿using Plarium_ValeriyShcherba.Scripts.Data.Constants;
namespace Plarium_ValeriyShcherba.Scripts.Forms.FilesDiscovery
{
    partial class FilesDiscovery
    {
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.TreeView = new System.Windows.Forms.TreeView();
            this.ButtonSelectDirectory = new System.Windows.Forms.Button();
            this.ButtonSelectXML = new System.Windows.Forms.Button();
            this.ButtonStart = new System.Windows.Forms.Button();
            this.fromDirectoryLabel = new System.Windows.Forms.Label();
            this.toDirectoryLabel = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
      
            this.TreeView.Location = new System.Drawing.Point(12, 12);
            this.TreeView.Name = "TreeView";
            this.TreeView.Size = new System.Drawing.Size(168, 402);
            this.TreeView.TabIndex = 0;
     
            this.ButtonSelectDirectory.Location = new System.Drawing.Point(186, 12);
            this.ButtonSelectDirectory.Name = "ButtonSelectDirectory";
            this.ButtonSelectDirectory.Size = new System.Drawing.Size(125, 23);
            this.ButtonSelectDirectory.TabIndex = 1;
            this.ButtonSelectDirectory.Text = "Select directory";
            this.ButtonSelectDirectory.UseVisualStyleBackColor = true;
            this.ButtonSelectDirectory.Click += new System.EventHandler(this.ButtonSelectDirectory_Click);
        
            this.ButtonSelectXML.Location = new System.Drawing.Point(186, 42);
            this.ButtonSelectXML.Name = "ButtonSelectXML";
            this.ButtonSelectXML.Size = new System.Drawing.Size(125, 23);
            this.ButtonSelectXML.TabIndex = 2;
            this.ButtonSelectXML.Text = "Select path to xml";
            this.ButtonSelectXML.UseVisualStyleBackColor = true;
            this.ButtonSelectXML.Click += new System.EventHandler(this.ButtonSelectXML_Click);
          
            this.ButtonStart.Location = new System.Drawing.Point(186, 72);
            this.ButtonStart.Name = "Start";
            this.ButtonStart.Size = new System.Drawing.Size(125, 23);
            this.ButtonStart.TabIndex = 3;
            this.ButtonStart.Text = "Start";
            this.ButtonStart.UseVisualStyleBackColor = true;
            this.ButtonStart.Click += new System.EventHandler(this.ButtonStart_Click);
        
            this.fromDirectoryLabel.AutoSize = true;
            this.fromDirectoryLabel.Location = new System.Drawing.Point(317, 17);
            this.fromDirectoryLabel.Name = "fromDirectoryLabel";
            this.fromDirectoryLabel.Size = new System.Drawing.Size(0, 13);
            this.fromDirectoryLabel.TabIndex = 4;
    
            this.toDirectoryLabel.AutoSize = true;
            this.toDirectoryLabel.Location = new System.Drawing.Point(317, 47);
            this.toDirectoryLabel.Name = "toDirectoryLabel";
            this.toDirectoryLabel.Size = new System.Drawing.Size(0, 13);
            this.toDirectoryLabel.TabIndex = 5;
   
            this.saveFileDialog.DefaultExt = "xml";
 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 426);
            this.Controls.Add(this.toDirectoryLabel);
            this.Controls.Add(this.fromDirectoryLabel);
            this.Controls.Add(this.ButtonStart);
            this.Controls.Add(this.ButtonSelectXML);
            this.Controls.Add(this.ButtonSelectDirectory);
            this.Controls.Add(this.TreeView);
            this.Name = "Form1";
            this.Text = AppConstants.APPLICATION_NAME;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.TreeView TreeView;
        private System.Windows.Forms.Button ButtonSelectDirectory;
        private System.Windows.Forms.Button ButtonSelectXML;
        private System.Windows.Forms.Button ButtonStart;
        private System.Windows.Forms.Label fromDirectoryLabel;
        private System.Windows.Forms.Label toDirectoryLabel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}


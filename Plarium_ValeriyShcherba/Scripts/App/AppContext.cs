﻿using Plarium_ValeriyShcherba.Scripts.App.Scopes;
using Plarium_ValeriyShcherba.Scripts.App.Services;
using Plarium_ValeriyShcherba.Scripts.App.Threads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Plarium_ValeriyShcherba.Scripts.App
{
   public class AppContext
    {
        public static AppContext Instance;

        public event Action ContextInitialized;

        private AppServiceScope _appServiceScope;

        public ThreadManager ThreadManager { get { return _appServiceScope.ThreadManager; } }

        public void Initialize()
        {
            Instance = this;

            _appServiceScope = new AppServiceScope();
            _appServiceScope.Initialized += AppScope_InitializedEvent;

            _appServiceScope.Initialize(this);
        }

        public void Release()
        {
            _appServiceScope.Initialized -= AppScope_InitializedEvent;
            _appServiceScope.Release();
            _appServiceScope = null;

            Instance = null;
        }

        private void AppScope_InitializedEvent()
        {
            if (_appServiceScope.IsInitialized)
            {
                OnContextInitialized();
            }
        }

        private void OnContextInitialized()
        {
            if (ContextInitialized != null)
            {
                ContextInitialized();
            }
        }
    }
}

﻿using Plarium_ValeriyShcherba.Scripts.App;
using Plarium_ValeriyShcherba.Scripts.Forms.FilesDiscovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Plarium_ValeriyShcherba.Scripts.App
{
    static class AppRoot
    {
        public static AppContext _appContext;

        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ApplicationExit += new EventHandler(Application_ApplicationExitEvent);

            _appContext = new AppContext();
            _appContext.ContextInitialized += AppContext_InitializedEvent;
            _appContext.Initialize();
        }

        private static void Application_ApplicationExitEvent(object sender, EventArgs e)
        {
            _appContext.Release();
            _appContext = null;
        }

        private static void AppContext_InitializedEvent()
        {
            _appContext.ContextInitialized -= AppContext_InitializedEvent;

            Application.Run(new FilesDiscovery());
        }
    }
}

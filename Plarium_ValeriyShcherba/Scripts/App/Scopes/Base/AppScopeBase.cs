﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plarium_ValeriyShcherba.Scripts.App.Scopes.Base
{
    public class AppScopeBase
    {
        public event Action Initialized;

        protected bool _isInitialized;

        public bool IsInitialized { get { return _isInitialized; } }

        public virtual void Initialize(AppContext appContext) { }
        public virtual void Release() { }

        protected void OnInitialized()
        {
            if (Initialized != null)
            {
                Initialized();
            }
        }
    }
}

﻿using Plarium_ValeriyShcherba.Scripts.App.Scopes.Base;
using Plarium_ValeriyShcherba.Scripts.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plarium_ValeriyShcherba.Scripts.App.Scopes
{
    public class AppServiceScope: AppScopeBase
    {
        private ThreadManager _threadManager;
        public ThreadManager ThreadManager { get { return _threadManager; } }

        public override void Initialize(AppContext appContext)
        {
            _threadManager = new ThreadManager();
            _threadManager.Initialize(appContext);

            _isInitialized = true;
            OnInitialized();
        }

        public override void Release()
        {
            _threadManager.Release();

            _threadManager = null;
        }
    }
}

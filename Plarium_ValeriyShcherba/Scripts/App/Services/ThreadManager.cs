﻿using Plarium_ValeriyShcherba.Scripts.App.Scopes.Base;
using Plarium_ValeriyShcherba.Scripts.App.Threads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Plarium_ValeriyShcherba.Scripts.App.Services
{
   public class ThreadManager
    {
        public ManualResetEvent WaitFillHandle;

        private DetectFilesThread _detectFilesThread;
        private CreateTreeThread _createTreeThread;
        private WriteXmlThread _writeXmlThread;

        public DetectFilesThread DetectFilesThread { get { return _detectFilesThread; } }
        public CreateTreeThread TreeThread { get { return _createTreeThread; } }
        public WriteXmlThread WriteXmlThread { get { return _writeXmlThread; } }

        public void Initialize(AppContext appContext)
        {
            _detectFilesThread = new DetectFilesThread();
            _createTreeThread = new CreateTreeThread();
            _writeXmlThread = new WriteXmlThread();

            _detectFilesThread.Initialize(appContext);
            _createTreeThread.Initialize(appContext);
            _writeXmlThread.Initialize(appContext);
        }

        public void Release()
        {
            _detectFilesThread.Release();
            _createTreeThread.Release();
            _writeXmlThread.Release();

            _detectFilesThread = null;
            _createTreeThread = null;
            _writeXmlThread = null;
        }

        public void StopAllThreads()
        {
            _detectFilesThread.Stop();
            _createTreeThread.Stop();
            _writeXmlThread.Stop();
        }
    }
}

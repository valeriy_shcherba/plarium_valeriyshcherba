﻿using Plarium_ValeriyShcherba.Scripts.App.Threads.Base;
using Plarium_ValeriyShcherba.Scripts.Data.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Plarium_ValeriyShcherba.Scripts.App.Threads
{
    public class DetectFilesThread: CustomThreadBase
    {
        public event Action<DirectoryInfo, FileInfo> FileFound;
        public event Action<TreeNode> TreeNodeFound;
        public event Action<DirectoryInfo> DirectoryClosed;
        public event Action ThreadCompleted;

        private string _pathDirectory;

        public void Start(string pathDirectory)
        {
            _pathDirectory = pathDirectory;

            _thread = new Thread(new ThreadStart(StartThread));
            _thread.Start();
        }

        public void Stop()
        {
            if (_thread != null)
            {
                _thread.Abort();
            }
        }

        protected override void StartThread()
        {
            base.StartThread();

            _threadManager.WaitFillHandle.WaitOne();
            GetFilesAndDirectories(new DirectoryInfo(_pathDirectory));
            OnThreadCompleted();

            _threadManager.StopAllThreads();
        }

        protected override void StopThread()
        {
            base.StopThread();
        }

        private TreeNode GetFilesAndDirectories(DirectoryInfo directoryInfo)
        {
            try
            {
                var directoryNode = new TreeNode(directoryInfo.Name);
                OnFileFound(directoryInfo, null);

                if (GetAccess(directoryInfo))
                {
                    foreach (var directory in directoryInfo.GetDirectories())
                    {

                        directoryNode.Nodes.Add(GetFilesAndDirectories(directory));
                    }
                    foreach (var file in directoryInfo.GetFiles())
                    {
                        OnFileFound(null, new FileInfo(file.FullName));
                        directoryNode.Nodes.Add(new TreeNode(file.Name));
                    }
                }

                TreeNode node = directoryNode;
                OnTreeNodeFound(node);

                _threadManager.WaitFillHandle.WaitOne();
                _threadManager.WaitFillHandle = new ManualResetEvent(false);


                OnDirectoryClosed(directoryInfo);
                return directoryNode;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, MessageConstants.MESSAGE_ERROR);
                _threadManager.StopAllThreads();

                return GetFilesAndDirectories(directoryInfo);
            }
        }

        private bool GetAccess(DirectoryInfo info)
        {
            try
            {
                DirectorySecurity DS = info.GetAccessControl();
                bool WriteAccess = false;

                foreach (FileSystemAccessRule Rule in DS.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
                {
                    if ((Rule.FileSystemRights & FileSystemRights.Read) != 0)
                    {
                        WriteAccess = true;
                    }
                }
                return WriteAccess;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, MessageConstants.MESSAGE_ERROR);
                _threadManager.StopAllThreads();

                return false;
            }
        }

        private void OnFileFound(DirectoryInfo directoryInfo, FileInfo fileInfo)
        {
            if (FileFound != null)
            {
                FileFound(directoryInfo, fileInfo);
            }
        }

        private void OnTreeNodeFound(TreeNode treeNode)
        {
            if (TreeNodeFound != null)
            {
                TreeNodeFound(treeNode);
            }
        }

        private void OnDirectoryClosed(DirectoryInfo directoryInfo)
        {
            if (DirectoryClosed != null)
            {
                DirectoryClosed(directoryInfo);
            }
        }

        private void OnThreadCompleted()
        {
            if (ThreadCompleted != null)
            {
                ThreadCompleted();
            }
        }
    }
}

﻿using Plarium_ValeriyShcherba.Scripts.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Plarium_ValeriyShcherba.Scripts.App.Threads.Base
{
    public class CustomThreadBase
    {
        protected Thread _thread;

        protected ThreadManager _threadManager;

        public virtual void Initialize (AppContext appContext)
        {
            _threadManager = appContext.ThreadManager;
        }

        public virtual void Release()
        {
            _threadManager = null;
            _thread = null;
        }

        protected virtual void StartThread() { }
        protected virtual void StopThread() { }
    }
}

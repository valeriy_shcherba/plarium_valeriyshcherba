﻿using Plarium_ValeriyShcherba.Scripts.App.Threads.Base;
using Plarium_ValeriyShcherba.Scripts.Data.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Plarium_ValeriyShcherba.Scripts.App.Threads
{
    public class WriteXmlThread: CustomThreadBase
    {
        private StreamWriter _xmlWriter;

        private string _pathXML;

        public void Start(string pathXML)
        {
            _pathXML = pathXML;
            _thread = new Thread(new ThreadStart(StartThread));
            _thread.Start();
        }

        public void Stop()
        {
            StopThread();
        }

        protected override void StartThread()
        {
            base.StartThread();

            try
            {
                _xmlWriter = new StreamWriter(_pathXML);
                _xmlWriter.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");

                _threadManager.DetectFilesThread.FileFound += FinderThread_FoundFileEvent;
                _threadManager.DetectFilesThread.DirectoryClosed += FinderThread_DirectoryClosedEvent;
                _threadManager.DetectFilesThread.ThreadCompleted += FinderThread_CompletedEvent;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, MessageConstants.MESSAGE_ERROR);
                _threadManager.StopAllThreads();
            }
        }

        protected override void StopThread()
        {
            base.StopThread();

            _threadManager.DetectFilesThread.FileFound += FinderThread_FoundFileEvent;
            _threadManager.DetectFilesThread.DirectoryClosed += FinderThread_DirectoryClosedEvent;
            _threadManager.DetectFilesThread.ThreadCompleted += FinderThread_CompletedEvent;

            if (_thread != null)
            {
                _thread.Abort();
            }
        }

        private void FinderThread_FoundFileEvent(DirectoryInfo dInfo, FileInfo fInfo)
        {
            if (fInfo != null)
            {

                float fileSize = (fInfo.Length / 1024f);
                
                _xmlWriter.WriteLine("<File>");
                _xmlWriter.WriteLine("<Name>" + fInfo.Name + "</Name>");
                _xmlWriter.WriteLine("<CreationDate>" + fInfo.CreationTime + "</CreationDate>");
                _xmlWriter.WriteLine("<AccessDate>" + fInfo.LastAccessTime + "</AccessDate>");
                _xmlWriter.WriteLine("<ModificationDate>" + fInfo.LastWriteTime + "</ModificationDate>");
                _xmlWriter.WriteLine("<Attributes>" + fInfo.Attributes.ToString() + "</Attributes>");
                _xmlWriter.WriteLine("<Size>" + BytesToString(fInfo.Length) + "</Size>");
                _xmlWriter.WriteLine("<Owner>" + GetOwner(fInfo, null) + "</Owner>");
                _xmlWriter.WriteLine("<Permissions>" + GetAccess(fInfo, null) + "</Permissions>");
                _xmlWriter.WriteLine("</File>");
            }

            if (dInfo != null)
            {
                _xmlWriter.WriteLine("<Directory>");
                _xmlWriter.WriteLine("<Name>" + dInfo.Name + "</Name>");
                _xmlWriter.WriteLine("<CreationDate>" + dInfo.CreationTime + "</CreationDate>");
                _xmlWriter.WriteLine("<AccessDate>" + dInfo.LastAccessTime + "</AccessDate>");
                _xmlWriter.WriteLine("<ModificationDate>" + dInfo.LastWriteTime + "</ModificationDate>");
                _xmlWriter.WriteLine("<Attributes>" + dInfo.Attributes.ToString() + "</Attributes>");
                _xmlWriter.WriteLine("<Owner>" + GetOwner(null, dInfo) + "</Owner>");
                _xmlWriter.WriteLine("<Size>" + BytesToString(GetDirSize(dInfo)) + "</Size>");
                _xmlWriter.WriteLine("<Permissions>" + GetAccess(null, dInfo) + "</Permissions>");
            }
        }

        private void FinderThread_CompletedEvent()
        {
            _xmlWriter.Close();
        }

        private String BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 2);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }

        private String GetOwner(FileInfo file, DirectoryInfo directory)
        {
            if (file != null)
            {
                var fs = file.GetAccessControl();

                var sid = fs.GetOwner(typeof(SecurityIdentifier));
                return sid.Value;
            }
            else
            {
                var fs = directory.GetAccessControl();

                var sid = fs.GetOwner(typeof(SecurityIdentifier));
                return sid.Value;
            }
        }

        private long GetDirSize(DirectoryInfo d)
        {
            long size = 0;

            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }

            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += GetDirSize(di);
            }

            return size;
        }

        private string GetAccess(FileInfo file, DirectoryInfo directory)
        {
            string result = String.Empty;

            if (file != null)
            {
                var accessControlList = file.GetAccessControl();
                var accessRules = accessControlList.GetAccessRules(true, true,
                                typeof(System.Security.Principal.SecurityIdentifier));
                if (accessRules == null)
                {
                    result = MessageConstants.MESSAGE_NOTHINK;
                }
                else
                {
                    if (accessRules.Count != 0)
                    {
                        result = ((FileSystemAccessRule)accessRules[0]).FileSystemRights.ToString();
                    }
                    else
                    {
                        result = MessageConstants.MESSAGE_NOTHINK;
                    }
                }
            }
            else
            {
                var accessControlList = directory.GetAccessControl();
                var accessRules = accessControlList.GetAccessRules(true, true,
                                typeof(System.Security.Principal.SecurityIdentifier));
                if (accessRules == null)
                {
                    result = MessageConstants.MESSAGE_NOTHINK;
                }
                else
                {
                    if (accessRules.Count != 0)
                    {
                        result = ((FileSystemAccessRule)accessRules[0]).FileSystemRights.ToString();
                    }
                    else
                    {
                        result = MessageConstants.MESSAGE_NOTHINK;
                    }
                }
            }

            return result;
        }

        private void FinderThread_DirectoryClosedEvent(DirectoryInfo dInfo)
        {
            if (dInfo != null)
            {
                _xmlWriter.WriteLine("</Directory>");
            }
        }
    }
}

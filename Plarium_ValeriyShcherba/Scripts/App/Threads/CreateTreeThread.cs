﻿using Plarium_ValeriyShcherba.Scripts.App.Services;
using Plarium_ValeriyShcherba.Scripts.App.Threads.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Plarium_ValeriyShcherba.Scripts.App.Threads
{
   public class CreateTreeThread: CustomThreadBase
    {
        private TreeView _treeView;

        public void Start(TreeView treeView)
        {
            _treeView = treeView;

            _thread = new Thread(new ThreadStart(StartThread));
            _thread.Start();
        }

        public void Stop()
        {
            StopThread();

            _treeView = null;
        }

        protected override void StartThread()
        {
            base.StartThread();

           _threadManager.DetectFilesThread.TreeNodeFound += FinderThread_TreeNodeFoundEvent;

           _threadManager.WaitFillHandle.Set();
        }

        protected override void StopThread()
        {
            base.StopThread();

            _threadManager.DetectFilesThread.TreeNodeFound -= FinderThread_TreeNodeFoundEvent;

            if (_thread != null)
            {
                _thread.Abort();
            }
        }

        private void FinderThread_TreeNodeFoundEvent(TreeNode node)
        {
            TreeNode n = node.Clone() as TreeNode;
            _treeView.Invoke((MethodInvoker)(() =>  {

                _treeView.Nodes.Clear();
                _treeView.Nodes.Insert(0, n);

            }));

            _threadManager.WaitFillHandle.Set();
        }
    }
}

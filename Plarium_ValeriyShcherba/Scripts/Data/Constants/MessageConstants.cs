﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plarium_ValeriyShcherba.Scripts.Data.Constants
{
    class MessageConstants
    {
        public static string MESSAGE_ERROR = "Error!";
        public static string MESSAGE_NOT_EXIST_DIRECTORY = "Path to directory is not exist!";
        public static string MESSAGE_NOT_EXIST_XML = "Path to XML is not exist!";
        public static string MESSAGE_NOTHINK = "NOTHINK!";
    }
}

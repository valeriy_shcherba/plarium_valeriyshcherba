﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plarium_ValeriyShcherba.Scripts.Data.Constants
{
    class AppConstants
    {
        public static string APPLICATION_NAME = "Plarium_ValeriyShcherba - Files Discovery";
        public static string FILE_FILTER_XML = "Extensible Markup Language (*.xml)|*.xml";
    }
}
